import 'dart:math';

String isPrime(int num) {
  if (num <= 1) {
    return '$num is not prime number';
  } else if (num == 2) {
    return '$num is prime number';
  } else if (num % 2 == 0) {
    return '$num is not prime number';
  }
  for (int i = 3; i <= sqrt(num); i += 2) {
    if (num % i == 0) {
      return '$num is not prime number';
    }
  }
  return '$num is prime number';
}


